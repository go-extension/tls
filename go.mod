module gitlab.com/go-extension/tls

go 1.24.0

require golang.org/x/crypto v0.33.0

require (
	github.com/andybalholm/brotli v1.1.1
	github.com/blang/semver/v4 v4.0.0
	github.com/klauspost/compress v1.17.11
	github.com/rogpeppe/go-internal v1.9.0
	golang.org/x/sys v0.30.0
)

require (
	github.com/RyuaNerin/go-krypto v1.3.0
	github.com/pedroalbanese/camellia v0.0.0-20220911183557-30cc05c20118
	github.com/pmorjan/kmod v1.1.1
	gitlab.com/go-extension/aes-ccm v0.0.0-20230221065045-e58665ef23c7
	gitlab.com/go-extension/hpke v0.0.0-20250212195157-716075a00b8a
	gitlab.com/go-extension/mlkem768 v0.0.0-20240814071630-937354a2177e
	gitlab.com/go-extension/rand v0.0.0-20240303103951-707937a049b5
)
